import 'package:apicallsproduct/database/user_database.dart';
import 'package:apicallsproduct/models/user.dart';

//helper class in order to communicate with our SQLite database
class UserHelper {
  final dbProvider = DatabaseProvider.dbProvider;

  Future<int> createUser(Map<String, dynamic> values) async {
    final db = await dbProvider.database;

    var result = db.insert(userTable, values);
    return result;
  }

  Future<int> deleteUser(int id) async {
    final db = await dbProvider.database;

    var result = db.delete(userTable, where: "id = ?", whereArgs: [id]);
    return result;
  }

  Future<String> getToken(int id) async {
    final db = await dbProvider.database;
    try {
      List<Map> users =
          await db.query(userTable, where: "id = ?", whereArgs: [id]);
      return users[0]["token"];
    } catch (e) {
      print(e);
    }
  }

  Future<bool> checkUser(int id) async {
    final db = await dbProvider.database;

    try {
      List<Map> users =
          await db.query(userTable, where: "id = ?", whereArgs: [id]);
      // print(users); //print data in database
      if (users.length > 0) {
        return true;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  }
}
