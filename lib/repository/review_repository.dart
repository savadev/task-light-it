import 'package:apicallsproduct/models/review.dart';
import 'package:apicallsproduct/networking/ApiProvider.dart';

class ReviewRepository {
  ApiProvider _provider = ApiProvider();

  Future<List<Review>> fetchReviewById(String product_id) async {
    final response = await _provider.get("api/reviews/" + product_id);
    return response.map<Review>((json) => Review.fromJson(json)).toList();
  }

  Future<Review> sendReviewById(String product_id, String jsonString, String token) async {
    final response = await _provider.post("api/reviews/" + product_id, jsonString, token);
    return Review.fromJsonStatus(response);
  }
}