import 'package:apicallsproduct/helper/user_helper.dart';
import 'package:apicallsproduct/models/token.dart';
import 'package:apicallsproduct/models/user.dart';
import 'package:apicallsproduct/networking/ApiProvider.dart';


class AuthorizationRepository {
  final userHelper = UserHelper();
  ApiProvider _provider = ApiProvider();

  //get token from api when user login ir register
  Future<Token> getToken(String request, String jsonString) async {
    final response = await _provider.post("/api/" + request, jsonString, "");
    return Token.fromJson(response);
  }

  //write user to the database
  Future<void> persistToken(Map<String, dynamic> userData, String token) async {
    Map<String, dynamic> values = {"id" : 0, "username" : userData["username"], "token" : token};
    int r = await userHelper.createUser(values);
  }

  //delete user from database
  Future<void> deleteToken() async {
    await userHelper.deleteUser(0);
  }


  //checks the database for the user 
  Future<bool> hasToken() async {
    bool result = await userHelper.checkUser(0);
    return result;
  }

}