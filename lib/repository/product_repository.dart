import 'package:apicallsproduct/models/product.dart';
import 'package:apicallsproduct/networking/ApiProvider.dart';

class ProductRepository {
  ApiProvider _provider = ApiProvider();

  Future<List<Product>> fetchProductData() async {
    final response = await _provider.get("api/products/");
    return response.map<Product>((json) => Product.fromJson(json)).toList();
// Product.fromJson(response);
  }
}