class Product {
  final int id;
  final String title;
  final String image;
  final String text;

  Product({this.id, this.title, this.image, this.text});

  factory Product.fromJson(Map<String, dynamic> json) {
    return Product(
      id: json['id'],
      title: json['title'],
      image: json['img'],
      text: json['text'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    data['title'] = this.title;
    data['image'] = this.image;
    data['text'] = this.text;
    return data;
  }
}
