class User {
  // final String id;
  final String username;
  final String password;

  User({
    // this.id,
    this.username,
    this.password,
  });

  factory User.fromJson(Map<String, dynamic> json) {
    return User(
      // id: json['id'],
      username: json['username'],
      password: json['password'],
    );
  }

  Map<String, String> toJson() {
    final Map<String, String> data = Map<String, String>();
    // data['id'] = this.id;
    data['username'] = this.username;
    data['password'] = this.password;
    return data;
  }
}
