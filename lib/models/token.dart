class Token {
  final bool success;
  final String message;
  final String token;

  Token({this.success, this.token, this.message});

  factory Token.fromJson(Map<String, dynamic> json) {
    return Token(
      success: json['success'],
      message: json['message'],
      token: json['token'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['success'] = this.success;
    data['token'] = this.token;
    return data;
  }
}