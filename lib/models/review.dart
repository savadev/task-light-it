class Review {
  final int id;
  final int product;
  final int rate;
  final String text;
  final Map<String, dynamic> created_by;
  final String created_at;
  final bool success;

  Review({this.id, this.product, this.rate, this.text, this.created_by, this.created_at, this.success});

  factory Review.fromJson(Map<String, dynamic> json) {
    return Review(
      id: json['id'],
      product: json['product'],
      rate: json['rate'],
      text: json['text'],
      created_by: Map<String, dynamic>.from(json['created_by']),
      created_at: json['created_at'],
      success: json['success'],
    );
  }

  factory Review.fromJsonStatus(Map<String, dynamic> json) {
    return Review(
      success: json['success'],
    );
  }

  Map<String, String> toJson() {
    final Map<String, String> data = Map<String, String>();
    data['rate'] = this.rate.toString();
    data['text'] = this.text;
    return data;
  }
}
