import 'dart:async';

import 'package:apicallsproduct/repository/authorization_repository.dart';
import 'package:apicallsproduct/view/ProductsScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  AuthorizationRepository _authorizationRepository;
  AnimationController animationController;
  Animation<double> animation;
  bool _ifAuthorized;
  var _visible = true;

  startTime() async {
    var _duration = Duration(seconds: 2);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.pushAndRemoveUntil(
        context,
        PageRouteBuilder(
          pageBuilder: (context, animation1, animation2) => GetProducts(
            ifAuthorized: _ifAuthorized,
          ),
          transitionDuration: Duration(seconds: 0),
        ),
        (route) => false);
  }

  isAuthorization() async {
    _ifAuthorized = await _authorizationRepository.hasToken();
    startTime();
  }

  @override
  void initState() {
    super.initState();
    _authorizationRepository = AuthorizationRepository();
    animationController = new AnimationController(
        vsync: this, duration: new Duration(seconds: 2));
    animation =
        new CurvedAnimation(parent: animationController, curve: Curves.easeOut);

    animation.addListener(() => this.setState(() {}));
    animationController.forward();

    setState(() {
      _visible = !_visible;
    });
    isAuthorization();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: Center(
          child: SvgPicture.asset(
            'assets/logo.svg',
            width: animation.value * 200,
          ),
        ),
      ),
    );
  }
}
