import 'package:apicallsproduct/blocs/product_bloc.dart';
import 'package:apicallsproduct/models/product.dart';
import 'package:apicallsproduct/networking/Response.dart';
import 'package:apicallsproduct/repository/authorization_repository.dart';
import 'package:apicallsproduct/view/widget/LoadingWidget.dart';
import 'package:apicallsproduct/view/widget/ErrorWidget.dart';
import 'package:apicallsproduct/view/NavDrawer.dart';
import 'package:apicallsproduct/view/ProductDetail.dart';
import 'package:apicallsproduct/view/widget/ProductWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class GetProducts extends StatefulWidget {
  final bool ifAuthorized;

  GetProducts({this.ifAuthorized});

  @override
  _GetProductsState createState() => _GetProductsState();
}

class _GetProductsState extends State<GetProducts> {
  ProductBloc _bloc;
  AuthorizationRepository _authorizationRepository = AuthorizationRepository();
  

  @override
  void initState() {
    super.initState();
    _bloc = ProductBloc();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: NavDrawer(
        ifAuthorized: widget.ifAuthorized,
      ),
      appBar: AppBar(
        elevation: 0.0,
        // automaticallyImplyLeading: false,
        title: SvgPicture.asset(
          'assets/logo.svg',
          height: 40,
        ),
        backgroundColor: Colors.black,
      ),
      backgroundColor: Colors.black,
      body: RefreshIndicator(
        onRefresh: () => _bloc.fetchProducts(),
        child: StreamBuilder<Response<List<Product>>>(
          stream: _bloc.productDataStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data.status) {
                case Status.LOADING:
                  return Loading(loadingMessage: snapshot.data.message);
                  break;
                case Status.COMPLETED:
                  return ProductsList(products: snapshot.data.data, ifAuthorized: widget.ifAuthorized);
                  break;
                case Status.ERROR:
                  return Error(
                    errorMessage: snapshot.data.message,
                    onRetryPressed: () => _bloc.fetchProducts(),
                  );
                  break;
              }
            }
            return Container();
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}

class ProductsList extends StatelessWidget {
  final List<Product> products;
  final bool ifAuthorized;
  const ProductsList({Key key, this.products, this.ifAuthorized}) : super(key: key);
  

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      padding: EdgeInsets.symmetric(horizontal: 10),
      itemCount: products.length,
      physics: BouncingScrollPhysics(),
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        childAspectRatio: 0.9,
        mainAxisSpacing: 0,
        crossAxisCount: 2,
        crossAxisSpacing: 15,
      ),
      itemBuilder: (context, index) {
        return InkWell(
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    ProductDetail(product: products[index], ifAuthorized: ifAuthorized,),
              ),
            );
          },
          child: ProductWidget(product: products[index]),
        );
      },
    );
  }
}
