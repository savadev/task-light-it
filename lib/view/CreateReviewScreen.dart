import 'dart:convert';

import 'package:apicallsproduct/blocs/send_review_bloc.dart';
import 'package:apicallsproduct/helper/user_helper.dart';
import 'package:apicallsproduct/models/product.dart';
import 'package:apicallsproduct/models/review.dart';
import 'package:apicallsproduct/networking/Response.dart';
import 'package:apicallsproduct/view/ProductDetail.dart';
import 'package:apicallsproduct/view/widget/LoadingWidget.dart';
import 'package:flutter/material.dart';

class CreateReview extends StatefulWidget {
  final Product product;

  CreateReview({this.product});
  @override
  _CreateReviewState createState() => _CreateReviewState();
}

class _CreateReviewState extends State<CreateReview> {
  final _userHelper = UserHelper();
  final _reviewController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  int rate = 0;

  _sendReview() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    if (formKey.currentState.validate()) {
      Review review = Review(rate: rate, text: _reviewController.text);
      String jsonString = jsonEncode(review.toJson());
      String token = await _userHelper.getToken(0);
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(
            builder: (context) => SendReview(
              product: widget.product,
              jsonString: jsonString,
              token: token,
            ),
          ),
          (route) => false);
    }
  }

  Widget _rate() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        IconButton(
            icon: Icon(Icons.star,
                color: rate > 0 ? Colors.yellow : Colors.grey, size: 40),
            onPressed: () {
              setState(() {
                rate = 1;
              });
            }),
        IconButton(
            icon: Icon(Icons.star,
                color: rate > 1 ? Colors.yellow : Colors.grey, size: 40),
            onPressed: () {
              setState(() {
                rate = 2;
              });
            }),
        IconButton(
            icon: Icon(Icons.star,
                color: rate > 2 ? Colors.yellow : Colors.grey, size: 40),
            onPressed: () {
              setState(() {
                rate = 3;
              });
            }),
        IconButton(
            icon: Icon(Icons.star,
                color: rate > 3 ? Colors.yellow : Colors.grey, size: 40),
            onPressed: () {
              setState(() {
                rate = 4;
              });
            }),
        IconButton(
            icon: Icon(Icons.star,
                color: rate > 4 ? Colors.yellow : Colors.grey, size: 40),
            onPressed: () {
              setState(() {
                rate = 5;
              });
            }),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusScope.of(context).requestFocus(new FocusNode());
      },
      child: Scaffold(
        backgroundColor: Color(0xffE5E3E3),
        appBar: AppBar(
          backgroundColor: Colors.black,
          title: Text("Create Review"),
          actions: [
            GestureDetector(
              onTap: () {
                _sendReview();
              },
              child: Padding(
                padding: const EdgeInsets.only(right: 20.0),
                child: Center(
                  child: Text(
                    "Send",
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
              ),
            ),
          ],
        ),
        body: ListView(
          shrinkWrap: true,
          physics: ClampingScrollPhysics(),
          children: [
            Column(
              children: [
                SizedBox(
                  height: 30,
                ),
                Container(
                  width: double.infinity,
                  color: Colors.white,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(
                          "Rate the product",
                          style: TextStyle(color: Colors.grey, fontSize: 18),
                        ),
                        _rate(),
                      ],
                    ),
                  ),
                ),
                SizedBox(
                  height: 30,
                ),
                Form(
                  key: formKey,
                  child: Container(
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 20, 20),
                      child: TextFormField(
                        controller: _reviewController,
                        decoration: InputDecoration(
                          labelText: 'Review',
                          labelStyle: TextStyle(color: Colors.grey),
                          counterText: "",
                        ),
                        cursorColor: Colors.black,
                        maxLength: 1000,
                        minLines: 1,
                        maxLines: 10,
                        keyboardType: TextInputType.text,
                        validator: (val) {
                          if (val.length == 0 || val.trim().length == 0) {
                            return "Review cannot be empty";
                          } else {
                            return null;
                          }
                        },
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class SendReview extends StatefulWidget {
  final Product product;
  final String jsonString;
  final String token;

  SendReview({this.product, this.jsonString, this.token});

  @override
  _SendReviewState createState() => _SendReviewState();
}

class _SendReviewState extends State<SendReview> {
  SendReviewBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = SendReviewBloc(
        widget.product.id.toString(), widget.jsonString, widget.token);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: StreamBuilder<Response<Review>>(
        stream: _bloc.sendReviewDataStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            switch (snapshot.data.status) {
              case Status.LOADING:
                return Loading(loadingMessage: snapshot.data.message);
                break;
              case Status.COMPLETED:
                return ProductDetail(
                  product: widget.product,
                  ifAuthorized: true,
                );
                break;
              case Status.ERROR:
                return ProductDetail(
                  product: widget.product,
                  ifAuthorized: true,
                );
                break;
            }
          }
          return Container();
        },
      ),
    );
  }
}
