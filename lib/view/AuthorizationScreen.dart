import 'dart:convert';

import 'package:apicallsproduct/blocs/authorization_bloc.dart';
import 'package:apicallsproduct/models/token.dart';
import 'package:apicallsproduct/models/user.dart';
import 'package:apicallsproduct/networking/Response.dart';
import 'package:apicallsproduct/view/ProductsScreen.dart';
import 'package:apicallsproduct/view/widget/LoadingWidget.dart';
import 'package:apicallsproduct/view/widget/ErrorWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';

class PreAuthorizationScreen extends StatefulWidget {

  @override
  _PreAuthorizationScreenState createState() => _PreAuthorizationScreenState();
}

class _PreAuthorizationScreenState extends State<PreAuthorizationScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.black,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SvgPicture.asset(
              'assets/logo.svg',
              height: 60,
            ),
            Column(
              children: [
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AuthorizationScreen(
                          ifLogin: false,
                        ),
                      ),
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "I\'m a new user",
                            style: TextStyle(color: Colors.black),
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.black,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AuthorizationScreen(
                          ifLogin: true,
                        ),
                      ),
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                    child: Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            "I already have an account",
                            style: TextStyle(color: Colors.black),
                          ),
                          Icon(
                            Icons.arrow_forward_ios,
                            color: Colors.black,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ],
            ),
            GestureDetector(
              onTap: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => GetProducts(
                        ifAuthorized: false,
                      ),
                    ),
                    (route) => false);
              },
              child: Text(
                "Skip",
                style: GoogleFonts.muli(
                    color: Colors.yellow,
                    fontWeight: FontWeight.w800,
                    fontSize: 20),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class AuthorizationScreen extends StatefulWidget {
  final bool ifLogin;

  AuthorizationScreen({Key key, this.ifLogin}) : super(key: key);

  @override
  _AuthorizationScreenState createState() => _AuthorizationScreenState();
}

class _AuthorizationScreenState extends State<AuthorizationScreen> {
  // AuthorizationBloc _bloc;
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.black,
        title: Text(widget.ifLogin ? "Sign In" : "Registration"),
      ),
      body: Container(
        child: Form(
          child: Padding(
            padding: EdgeInsets.all(40.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'username', icon: Icon(Icons.person)),
                  controller: _usernameController,
                ),
                TextFormField(
                  decoration: InputDecoration(
                      labelText: 'password', icon: Icon(Icons.security)),
                  controller: _passwordController,
                  obscureText: true,
                ),
                Container(
                  width: MediaQuery.of(context).size.width * 0.85,
                  height: MediaQuery.of(context).size.width * 0.22,
                  child: Padding(
                    padding: EdgeInsets.only(top: 30.0),
                    child: RaisedButton(
                      onPressed: () {
                        User user = User(
                          username: _usernameController.text,
                          password: _passwordController.text,
                        );
                        String jsonString = jsonEncode(user.toJson());
                        // print(jsonString);
                        Navigator.pushAndRemoveUntil(
                            context,
                            MaterialPageRoute(
                              builder: (context) => GetToken(
                                request:
                                    widget.ifLogin ? 'login/' : 'register/',
                                jsonString: jsonString,
                              ),
                            ),
                            (route) => false);
                      },
                      child: Text(
                        widget.ifLogin ? 'Login' : 'Register',
                        style: TextStyle(
                          fontSize: 24.0,
                        ),
                      ),
                      shape: StadiumBorder(
                        side: BorderSide(
                          color: Colors.black,
                          width: 2,
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class GetToken extends StatefulWidget {
  final String request;
  final String jsonString;

  GetToken({this.request, this.jsonString});
  @override
  _GetTokenState createState() => _GetTokenState();
}

class _GetTokenState extends State<GetToken> {
  AuthorizationBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = AuthorizationBloc(widget.request, widget.jsonString);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFF202020),
      body: StreamBuilder<Response<Token>>(
        stream: _bloc.authorizationDataStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            switch (snapshot.data.status) {
              case Status.LOADING:
                return Loading(loadingMessage: snapshot.data.message);
                break;
              case Status.COMPLETED:
                return GetProducts(
                  ifAuthorized: true,
                );
                break;
              case Status.ERROR:
                return Error(
                    errorMessage: snapshot.data.message,
                    onRetryPressed: () => Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (context) => PreAuthorizationScreen(),
                        ),
                        (route) => false));
                break;
            }
          }
          return Container();
        },
      ),
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}
