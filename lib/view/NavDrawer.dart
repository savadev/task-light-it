import 'package:apicallsproduct/helper/user_helper.dart';
import 'package:apicallsproduct/repository/authorization_repository.dart';
import 'package:apicallsproduct/view/AuthorizationScreen.dart';
import 'package:apicallsproduct/view/ProductsScreen.dart';
import 'package:apicallsproduct/view/SplashScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class NavDrawer extends StatefulWidget {
  final bool ifAuthorized;
  final String username;

  NavDrawer({this.ifAuthorized, this.username});
  @override
  _NavDrawerState createState() => _NavDrawerState();
}

class _NavDrawerState extends State<NavDrawer> {
  AuthorizationRepository _authorizationRepository = AuthorizationRepository();

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          DrawerHeader(
            padding: EdgeInsets.fromLTRB(0, 24, 0, 8),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SvgPicture.asset(
                  'assets/logo.svg',
                  height: 60,
                ),
                widget.ifAuthorized
                    ? ListTile(
                        leading: Icon(
                          Icons.account_circle,
                          color: Colors.white,
                          size: 25,
                        ),
                        title: Text(
                          "Wellcome",
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        ),
                      )
                    : ListTile(
                        leading: Icon(
                          Icons.account_circle,
                          color: Colors.white,
                          size: 25,
                        ),
                        title: Text(
                          'Sign In',
                          style: TextStyle(color: Colors.white, fontSize: 18),
                        ),
                        onTap: () {
                          Navigator.pushAndRemoveUntil(
                              context,
                              MaterialPageRoute(
                                builder: (context) => PreAuthorizationScreen(),
                              ),
                              (route) => false);
                        },
                      ),
              ],
            ),
            decoration: BoxDecoration(
              color: Colors.black,
            ),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          // ListTile(
          //   leading: Icon(Icons.verified_user),
          //   title: Text('Profile'),
          //   onTap: () => {Navigator.of(context).pop()},
          // ),
          // ListTile(
          //   leading: Icon(Icons.settings),
          //   title: Text('Settings'),
          //   onTap: () => {Navigator.of(context).pop()},
          // ),
          if (widget.ifAuthorized)
            ListTile(
              leading: Icon(Icons.exit_to_app),
              title: Text('Logout'),
              onTap: () async {
                await _authorizationRepository.deleteToken();
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(
                      builder: (context) => GetProducts(
                        ifAuthorized: false,
                      ),
                    ),
                    (route) => false);
              },
            ),
        ],
      ),
    );
  }
}
