import 'package:apicallsproduct/blocs/get_review_bloc.dart';
import 'package:apicallsproduct/models/product.dart';
import 'package:apicallsproduct/models/review.dart';
import 'package:apicallsproduct/networking/Response.dart';
import 'package:apicallsproduct/view/CreateReviewScreen.dart';
import 'package:apicallsproduct/view/ProductsScreen.dart';
import 'package:apicallsproduct/view/widget/LoadingWidget.dart';
import 'package:apicallsproduct/view/widget/ErrorWidget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';

class ProductDetail extends StatefulWidget {
  final Product product;
  final bool ifAuthorized;

  const ProductDetail({this.product, this.ifAuthorized});

  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetail> {
  GetReviewBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = GetReviewBloc(widget.product.id.toString());
  }

  Widget _appBar() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          InkWell(
            onTap: () {
              Navigator.pushAndRemoveUntil(
                  context,
                  PageRouteBuilder(
                    pageBuilder: (context, animation1, animation2) =>
                        GetProducts(
                      ifAuthorized: widget.ifAuthorized,
                    ),
                    transitionDuration: Duration(seconds: 0),
                  ),
                  (route) => false);
            },
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(
                  color: Colors.black54,
                  style: BorderStyle.solid,
                ),
                borderRadius: BorderRadius.all(Radius.circular(13)),
                color: Colors.transparent,
              ),
              child: Padding(
                padding: EdgeInsets.all(10),
                child: Icon(
                  Icons.arrow_back_ios,
                  color: Colors.black54,
                  size: 20,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _productImage() {
    return Hero(
      tag: widget.product.image,
      child: Image.network(
        "http://smktesting.herokuapp.com/static/" + widget.product.image,
        fit: BoxFit.contain,
        width: MediaQuery.of(context).size.width * 0.7,
      ),
    );
  }

  Widget _detailWidget() {
    return DraggableScrollableSheet(
      maxChildSize: .8,
      initialChildSize: .53,
      minChildSize: .53,
      builder: (context, scrollController) {
        return Container(
          padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft: Radius.circular(40),
              topRight: Radius.circular(40),
            ),
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment(0.1, 0.9),
              colors: [
                Color(0xff252525),
                Color(0xff010101),
              ],
            ),
          ),
          child: SingleChildScrollView(
            physics: ClampingScrollPhysics(),
            controller: scrollController,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                SizedBox(height: 5),
                Container(
                  alignment: Alignment.center,
                  child: Container(
                    width: 50,
                    height: 5,
                    decoration: BoxDecoration(
                        gradient: LinearGradient(
                          begin: Alignment.centerLeft,
                          end: Alignment.centerRight,
                          colors: [
                            Color(0xffFED08E),
                            Color(0xff705222),
                          ],
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(10))),
                  ),
                ),
                SizedBox(height: 15),
                Container(
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        widget.product.title,
                        style: GoogleFonts.muli(
                            color: Colors.white,
                            fontSize: 25,
                            fontWeight: FontWeight.w800),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                _description(),
                SizedBox(
                  height: 20,
                ),
                _reviews(),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _description() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text(
          "Description",
          style: GoogleFonts.muli(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 14,
          ),
        ),
        SizedBox(height: 20),
        // Text(widget.product.text),
        Text(
          widget.product.text,
          style: TextStyle(color: Colors.grey),
        ),
      ],
    );
  }

  Widget _reviews() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              "Reviews",
              style: GoogleFonts.muli(
                color: Colors.white,
                fontWeight: FontWeight.bold,
                fontSize: 14,
              ),
            ),
            if (widget.ifAuthorized)
              IconButton(
                  icon: Icon(
                    Icons.edit,
                    color: Colors.white,
                    // size: 25,
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CreateReview(
                          product: widget.product,
                        ),
                      ),
                    );
                  })
          ],
        ),
        SizedBox(height: widget.ifAuthorized ? 10 : 20),
        Container(
          height: 500,
          child: StreamBuilder<Response<List<Review>>>(
            stream: _bloc.reviewDataStream,
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                switch (snapshot.data.status) {
                  case Status.LOADING:
                    return Loading(loadingMessage: snapshot.data.message);
                    break;
                  case Status.COMPLETED:
                    return ReviewList(reviews: snapshot.data.data);
                    break;
                  case Status.ERROR:
                    return Error(
                      errorMessage: snapshot.data.message,
                      onRetryPressed: () =>
                          _bloc.fetchReview(widget.product.id.toString()),
                    );
                    break;
                }
              }
              return Container();
            },
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.dark,
        child: SafeArea(
          child: Container(
            decoration: BoxDecoration(color: Colors.white),
            child: Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    _appBar(),
                    _productImage(),
                  ],
                ),
                _detailWidget()
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}

class ReviewList extends StatelessWidget {
  final List<Review> reviews;

  ReviewList({this.reviews});

  Widget _rate(int rate) {
    return Row(
      children: <Widget>[
        Icon(
          rate > 0 ? Icons.star : Icons.star_border,
          color: rate > 0 ? Colors.yellow : Colors.white,
          size: 17,
        ),
        Icon(
          rate > 1 ? Icons.star : Icons.star_border,
          color: rate > 1 ? Colors.yellow : Colors.white,
          size: 17,
        ),
        Icon(
          rate > 2 ? Icons.star : Icons.star_border,
          color: rate > 2 ? Colors.yellow : Colors.white,
          size: 17,
        ),
        Icon(
          rate > 3 ? Icons.star : Icons.star_border,
          color: rate > 3 ? Colors.yellow : Colors.white,
          size: 17,
        ),
        Icon(
          rate > 4 ? Icons.star : Icons.star_border,
          color: rate > 4 ? Colors.yellow : Colors.white,
          size: 17,
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: reviews.length,
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      itemBuilder: (context, index) {
        return Column(
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  reviews[index].created_by["username"],
                  style: GoogleFonts.muli(
                    fontSize: 17,
                    color: Colors.white,
                    fontWeight: FontWeight.w800,
                  ),
                ),
                Text(
                  DateTime.parse(reviews[index].created_at).day.toString() +
                      ' ' +
                      DateTime.parse(reviews[index].created_at)
                          .month
                          .toString() +
                      ' ' +
                      DateTime.parse(reviews[index].created_at).year.toString(),
                  style: TextStyle(color: Colors.white, fontSize: 13),
                )
              ],
            ),
            _rate(reviews[index].rate),
            SizedBox(
              height: 10,
            ),
            Container(
              width: double.infinity,
              child: Text(
                reviews[index].text,
                style: TextStyle(color: Colors.white),
              ),
            ),
            Divider(
              color: Colors.white,
            ),
          ],
        );
      },
    );
  }
}
