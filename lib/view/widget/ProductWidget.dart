import 'package:apicallsproduct/models/product.dart';
import 'package:flutter/material.dart';

class ProductWidget extends StatelessWidget {
  final Product product;

  ProductWidget({this.product});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(20)),
        boxShadow: <BoxShadow>[
          BoxShadow(color: Color(0xfff8f8f8), blurRadius: 5, spreadRadius: 1),
        ],
      ),
      margin: EdgeInsets.symmetric(vertical: 10),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Expanded(
                  child: Hero(
                    tag: product.image,
                    child: Image.network(
                      "http://smktesting.herokuapp.com/static/" + product.image,
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
                Text(
                  product.title,
                  style: TextStyle(fontWeight: FontWeight.bold),
                ),
              ],
            ),
          ],
        ),
      ),
    );
    // return Container(
    //   decoration: BoxDecoration(
    //     color: Colors.white,
    //     borderRadius: BorderRadius.only(
    //       topLeft: Radius.circular(20),
    //       topRight: Radius.circular(20),
    //     ),
    //     border: Border.all(
    //       color: Colors.grey[200],
    //       width: 1,
    //     ),
    //   ),
    //   width: 220,
    //   child: Column(
    //     crossAxisAlignment: CrossAxisAlignment.stretch,
    //     children: <Widget>[
    //       Expanded(
    //         child: Hero(
    //           tag: product.image,
    //           child: Container(
    //             decoration: BoxDecoration(
    //               borderRadius: BorderRadius.only(
    //                 topLeft: Radius.circular(20),
    //                 topRight: Radius.circular(20),
    //               ),
    //             ),
    // child: Image.network(
    //   "http://smktesting.herokuapp.com/static/" + product.image,
    //   fit: BoxFit.contain,
    // ),
    //           ),
    //         ),

    //         // Align(
    //         //   alignment: Alignment.topRight,
    //         //   child: Padding(
    //         //     padding: EdgeInsets.all(12),
    //         //     child: Container(
    //         //       height: 30,
    //         //       width: 30,
    //         //       decoration: BoxDecoration(
    //         //         shape: BoxShape.circle,
    //         //         color: pet.favorite ? Colors.red[400] : Colors.white,
    //         //       ),
    //         //       child: Icon(
    //         //         Icons.favorite,
    //         //         size: 16,
    //         //         color: pet.favorite ? Colors.white : Colors.grey[300],
    //         //       ),
    //         //     ),
    //         //   ),
    //         // ),
    //       ),
    //       Divider(),
    //       Padding(
    //         padding: EdgeInsets.all(5),
    //         child: Text(
    //           product.title,
    //           style:
    //               TextStyle(fontWeight: FontWeight.bold, color: Colors.black),
    //         ),
    //       ),
    //     ],
    //   ),
    // );
  }
}
