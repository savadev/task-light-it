import 'dart:async';

import 'package:apicallsproduct/models/product.dart';
import 'package:apicallsproduct/networking/Response.dart';
import 'package:apicallsproduct/repository/product_repository.dart';

class ProductBloc {
  ProductRepository _productRepository;
  StreamController _productDataController;

  StreamSink<Response<List<Product>>> get productListSink =>
      _productDataController.sink;
  
  Stream<Response<List<Product>>> get productDataStream => _productDataController.stream;

  ProductBloc() {
    _productDataController = StreamController<Response<List<Product>>>();
    _productRepository = ProductRepository();
    fetchProducts();
  }

  fetchProducts() async {
    productListSink.add(Response.loading('Getting Products'));
    try {
      List<Product> products = await _productRepository.fetchProductData();
      productListSink.add(Response.completed(products));
    } catch(e) {
      productListSink.add(Response.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _productDataController?.close();
  }
}
