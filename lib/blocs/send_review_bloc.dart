import 'dart:async';

import 'package:apicallsproduct/models/review.dart';
import 'package:apicallsproduct/networking/Response.dart';
import 'package:apicallsproduct/repository/review_repository.dart';

class SendReviewBloc {
  ReviewRepository _reviewRepository;
  StreamController _sendReviewDataController;
  bool _isStreaming;

  StreamSink<Response<Review>> get sendReviewDataSink =>
      _sendReviewDataController.sink;

  Stream<Response<Review>> get sendReviewDataStream =>
      _sendReviewDataController.stream;

  SendReviewBloc(String product_id, String jsonString, String token) {
    _sendReviewDataController = StreamController<Response<Review>>();
    _reviewRepository = ReviewRepository();
    _isStreaming = true;
    sendReview(product_id, jsonString, token);
  }

  sendReview(String product_id, String jsonString, String token) async {
    sendReviewDataSink.add(Response.loading('Sending review'));
    try {
      Review review =
          await _reviewRepository.sendReviewById(product_id, jsonString, token);
      if (_isStreaming) sendReviewDataSink.add(Response.completed(review));
    } catch (e) {
      if (_isStreaming) sendReviewDataSink.add(Response.error(e));
      print(e);
    }
  }

  dispose() {
    _isStreaming = false;
    _sendReviewDataController?.close();
  }
}
