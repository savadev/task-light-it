import 'dart:async';
import 'dart:convert';

import 'package:apicallsproduct/models/token.dart';
import 'package:apicallsproduct/networking/Response.dart';
import 'package:apicallsproduct/repository/authorization_repository.dart';

class AuthorizationBloc {
  AuthorizationRepository _authorizationRepository;
  StreamController _authorizationDataController;

  StreamSink<Response<Token>> get authorizationDataSink =>
      _authorizationDataController.sink;

  Stream<Response<Token>> get authorizationDataStream =>
      _authorizationDataController.stream;

  AuthorizationBloc(String request, String jsonString) {
    _authorizationDataController = StreamController<Response<Token>>();
    _authorizationRepository = AuthorizationRepository();
    fetchToken(request, jsonString);
  }

  fetchToken(String request, String jsonString) async {
    authorizationDataSink.add(Response.loading(
        request == 'login' ? 'Login process' : 'Register process'));
    try {
      Token token =
          await _authorizationRepository.getToken(request, jsonString);
      if (token.success == true) {
        await _authorizationRepository.persistToken(
            jsonDecode(jsonString), token.token);
        authorizationDataSink.add(Response.completed(token));
      } else {
        authorizationDataSink.add(Response.error(token.message));
      }
    } catch (e) {
      authorizationDataSink.add(Response.error(e));
      print(e);
    }
  }

  dispose() {
    _authorizationDataController?.close();
  }
}
