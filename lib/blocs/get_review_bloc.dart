import 'dart:async';

import 'package:apicallsproduct/models/review.dart';
import 'package:apicallsproduct/networking/Response.dart';
import 'package:apicallsproduct/repository/review_repository.dart';

class GetReviewBloc {
  ReviewRepository _reviewRepository;
  StreamController _reviewDataController;
  bool _isStreaming;

  StreamSink<Response<List<Review>>> get reviewDataSink => _reviewDataController.sink;

  Stream<Response<List<Review>>> get reviewDataStream => _reviewDataController.stream;

  GetReviewBloc(String product_id) {
    _reviewDataController = StreamController<Response<List<Review>>>();
    _reviewRepository = ReviewRepository();
    _isStreaming = true;
    fetchReview(product_id);
  }

  fetchReview(String product_id) async {
    reviewDataSink.add(Response.loading('Getting reviews'));
    try {
      List<Review> review = await _reviewRepository.fetchReviewById(product_id);
      if (_isStreaming) reviewDataSink.add(Response.completed(review));
    } catch(e) {
      if (_isStreaming) reviewDataSink.add(Response.error(e));
      print(e);
    }
  }

  dispose() {
    _isStreaming = false;
    _reviewDataController?.close();
  }
}
