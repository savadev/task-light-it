import 'package:apicallsproduct/networking/CustomException.dart';
import 'package:http/http.dart' as http;
import 'dart:io';
import 'dart:convert';
import 'dart:async';

class ApiProvider {
  final String _baseUrl = "http://smktesting.herokuapp.com/";

  Future<dynamic> get(String url) async {
    var responseJson;
    try {
      final response = await http.get(
        _baseUrl + url,
        // headers: {"Authorization": "Tonken $token"},
      );
      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  Future<dynamic> post(String url, String jsonString, String token) async {
    var responseJson;
    try {
      final response = await http.post(
        _baseUrl + url,
        // headers: {"Authorization": "Tonken $token"},
        headers: <String, String>{
          if (token != "") "Authorization": "Token $token",
          "Content-Type": "application/json; charset=UTF-8",
        },
        body: jsonString,
      );
      responseJson = _response(response);
    } on SocketException {
      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  dynamic _response(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        // print(responseJson);
        return responseJson;
      case 201:
        var responseJson = json.decode(response.body.toString());
        // print(responseJson);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:

      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:

      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
